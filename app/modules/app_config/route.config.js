define(['app'], function(app) {
    
    app.config(['$routeProvider',function($routeProvider){
        $routeProvider
        .when('/',{
            controller: 'mainCtrl',
            templateUrl: './app/modules/view/home.html'
        }).when('/home',{
            controller: 'mainCtrl',
            templateUrl: './app/modules/view/home.html'
        }).when('/precos',{
            controller: 'precoCtrl',
            templateUrl: './app/modules/view/preco.html'
        }).when('/destaque',{
            controller: 'destaqueCtrl',
            templateUrl: './app/modules/view/destaque.html'
        });
    }]);
    console.log('Routes module is running...');
});