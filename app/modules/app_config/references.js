
// Injeta todas a dependências..
define( function(require, factory) {
    'use strict';
    return[
        './../controllers/mainCtrl',
        './route.config',
        './../controllers/destaqueCtrl',
        './../controllers/precoCtrl'
    ]
});