//Configura e carrega minhas dependências em JS.
requirejs.config({

    paths:{
        'angular': './../../../bower_components/angular/angular',
        'angular-route':'./../../../bower_components/angular-route/angular-route',
        'jquery': './../../../bower_components/jquery/dist/jquery',
        'app': './app',
        'run':'./run'
    },
    shim:{
        'jquery': { exports: '$' },
        'app':{
            deps: ['jquery','angular-route']
        },
        'angular-route':{
            deps: ['angular']
        }
    }
});
// Chama modulo de execução da aplicação (Bootstrap Application).
require(["run"], function() {});