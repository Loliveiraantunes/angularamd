// Chama a aplicação principal com o modulo do angular e busca.
define(['app'], (app) =>{
    require(["./references"], function(deps) {
        require(deps,function() {
            angular.bootstrap(document , ['app']);
        });
    });
    console.log("App is running ..");
});